<!doctype html>
<html lang="en">
  <head>
    <title>TheWoosh Dashboard</title>
    <meta name="author" content="Tristan">
    <meta name="description" content="TheWoosh Dashboard shows information about software and services by TheWoosh.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <style>
      html {
        background-color: #F2F2F2;
      }
    </style>
  </head>
  <body>
    <main>
      <section id="products">
        <h1 class="section-title">Products</h1>
        <div>
          <article>
            <h1>MultiEngine</h1>
            <h2>Short description of the product.</h2>
          </article>
          <article>
            <h1>WebEngine</h1>
            <h2>Short description of the product.</h2>
            <div class="ci"><b>CI: </b><span style="background-color:red"></span></div>
          </article>
        </div>
      </section>
      <section id="services">
        <h1 class="section-title">Services</h1>
        <div>
          <?php
          $directoryName = "Services/";
          $directoryIterator = new DirectoryIterator($directoryName);
          foreach ($directoryIterator as $fileInfo) {
            if (!$fileInfo->isDot() && $fileInfo->getExtension() === 'xml') {
              $document = simplexml_load_file($directoryName . $fileInfo->getFilename());
              printf('<article><h1>%s</h1><h2>%s</h2><div class="service-status"><b>Status: </b><span class="%s">%s</span></div></article>',
                     $document->Name, $document->Description, $document->StatusClass, $document->StatusText);
            }
          }

          ?>
        </div>
      </section>
    </main>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
    <link href="Assets/Stylesheet.css" rel="stylesheet">
  </body>
</html>