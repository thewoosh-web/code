#!/usr/bin/python3

import socket
from enum import IntEnum

class Status(IntEnum):
     ONLINE = 0
     OFFLINE = 1

services_directory = "/var/www/html/code/Services/"
timeout = 2 # connect timeout in seconds

# For the rest of requests see wiki.vg/Protocol
def CheckServer(ip, port):
    sock = socket.socket()

    print("Trying server %s:%s..." % (ip, str(port)), end="")
    try:
        sock.settimeout(timeout)
        sock.connect((ip, port))
        sock.close()
        print("online")
        return Status.ONLINE
    except socket.error as exc:
        print("offline")
        print("Caught exception socket.error : %s" % exc)
        sock.close()
        return Status.OFFLINE

def WriteService(file_name, service_name, service_description, status):
    status_class = ("online", "offline")[int(status)]
    status_text = ("Online", "Offline")[int(status)]

    f = open(services_directory + file_name + ".xml", "w")
    f.write("<Service>")
    f.write("<Name>" + service_name + "</Name>")
    f.write("<Description>" + service_description + "</Description>")
    f.write("<StatusClass>" + status_class + "</StatusClass>")
    f.write("<StatusText>" + status_text + "</StatusText>")
    f.write("</Service>")
    f.close()

WriteService("mc-network-lobby",
             "Network (Lobby)",
             "This is the lobby where players can choose the game mode they wanna play.",
             CheckServer("play.thewoosh.me", 64800)
)

WriteService("mc-network-proxy",
             "Network (Proxy)",
             "This is the proxy connecting the servers (a.k.a. game modes) together.",
             CheckServer("play.thewoosh.me", 25565)
)

WriteService("mc-network-pvp",
             "Network (PvP)",
             "This is the PvP game mode on the TheWoosh Network.",
             CheckServer("116.203.142.155", 25565)
)

WriteService("mc-network-survival",
             "Network (Survival)",
             "This is the survival game mode on the TheWoosh Network.",
             CheckServer("159.69.221.74", 25565)
)
